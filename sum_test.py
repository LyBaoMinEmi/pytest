import sum

def test_sum_empty():
    assert sum.sum([]) == 0

def test_sum():
    assert sum.sum([1, 2, 3]) == 6

